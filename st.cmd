require essioc
require mrfioc2, 2.3.1+14
require evrisland, 2.4.0+0

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=OCTOPUS-010:RFS-EVR-101,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=OCTOPUS-010:RFS-EVR-101"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=OCTOPUS-010:RFS-EVR-101,$(EVREVTARGS=)"


afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=OCTOPUS-010:RFS-EVR-101, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=OCTOPUS-010:RFS-EVR-101")')



# Load snippet for evrisland
iocshLoad("$(evrisland_DIR)/evrisland-base.iocsh", "P=OCTOPUS-010:RFS-EVR-101")






# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")
